const path = require("path");

module.exports = {
  publicPath: "./",
  configureWebpack: {
    resolve: {
      alias: {
        "@components": path.resolve(__dirname, "src/components"),
        "@plugins": path.resolve(__dirname, "src/plugins"),
      },
      extensions: [".js", ".vue", ".json"],
    },
  },
};
