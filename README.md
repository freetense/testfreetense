<h1> Тестовое задание </h1>

## 1) Клонируйте репозиторий
```diff
git clone https://gitlab.com/freetense/testfreetense.git . 
```

## 2) Перейдите в папку с проектом

## 3) Установите yarn
```diff
npm install --global yarn
```

## 4) Для установки запустите команду
```diff
yarn install
```

## 5) Запустите команду разворачинвания сервера
```diff
yarn serve
```

## 6) После сборки откройте страницу http://localhost:8080/, откроется проект, кликните по кнопке.
![alt text](https://image.prntscr.com/image/PHrzTTSvRO6DKBk9pRh48A.png)

## 7)  Откроется окно верстки.
![alt text](https://image.prntscr.com/image/bNKPjwePSvmp0NCz63tYLA.png)
