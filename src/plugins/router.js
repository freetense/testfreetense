import Vue from "vue";
import VueRouter from "vue-router";

import Home from "@components/pages/Home.vue";
import Add from "@components/pages/Add.vue";

Vue.use(VueRouter);

export const routeNames = {
	home: "home",
	add: "add"
};

const routes = [
	{
		path: '*',
		name: routeNames.home,
		component: Home,
	},
	{
		path: '/add',
		name: routeNames.add,
		component: Add,
	},
];

export const router = new VueRouter({
	mode: 'history',
	routes,
});
