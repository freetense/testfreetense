import Vue from "vue";
import VuePageTransition from "vue-page-transition";
import App from "./App.vue";
import {router} from "@plugins/router";
import vuetify from "@plugins/vuetify";

Vue.use(VuePageTransition);

new Vue({
	router,
	vuetify,
	render: (h) => h(App),
}).$mount("#app");
